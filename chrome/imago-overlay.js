/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
let k8ImagoScope = {};
Components.utils.import("chrome://k8-imago-code/content/main.js", k8ImagoScope);
k8ImagoScope.setupGBrowser(gBrowser);

window.addEventListener("load", function () {
  k8ImagoScope.imagoCallWindowHook("load", window);
}, false);

window.addEventListener("unload", function () {
  k8ImagoScope.imagoCallWindowHook("unload", window);
}, false);
