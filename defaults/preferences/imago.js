pref("extensions.k8imago.minWidth", 1);
pref("extensions.k8imago.minHeight", 1);
pref("extensions.k8imago.maxWidth", 2600);
pref("extensions.k8imago.maxHeight", 2600);
pref("extensions.k8imago.showPlaceholder", true);
pref("extensions.k8imago.allowUnknownFormats", false);
pref("extensions.k8imago.maxLength", 0); /* in bytes, 0: no limit */
pref("extensions.k8imago.debugLog", false);
pref("extensions.k8imago.forgetSeconds", 20);
pref("extensions.k8imago.allowFirstPartyImages", true);
pref("extensions.k8imago.maxTotalBytesForDoc", 20971520); /* 20 megabytes */
