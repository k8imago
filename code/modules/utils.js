/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
// utility functions
var EXPORTED_SYMBOLS = [
  "MODULE_PATH",
  //
  "getDomWindowForChannel",
  "getByteCounterForDomWindow",
  "setByteCounterForDomWindow",
  "getBrowserForDomWindow",
  "createInputStreamFromString",
  "url2uri",
  "url2HashKey",
  "cssEscape",
  "oneShotTimer",
  "intervalTimer",
  //
  "setupGBrowser",
  "registerWindowHook",
  "imagoCallWindowHook",
  //
  "getRulesIFile",
  "getFileTextContents"
];

let {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;


//////////////////////////////////////////////////////////////////////////////
const MODULE_PATH = "chrome://k8-imago-code/content/modules/";


//////////////////////////////////////////////////////////////////////////////
let gBrowser = null;


function setupGBrowser (gb) {
  gBrowser = gb;
}


//////////////////////////////////////////////////////////////////////////////
// window hooks
//  string name (key)
//  array hooks (value)
let winhooks = {};

function registerWindowHook (name, cb) {
  if (typeof(name) !== "string" || !name) throw new Error("string hook name expected");
  if (typeof(cb) !== "function") throw new Error("function hook callback expected");
  if (name !== "load" && name !== "unload") throw new Error("invalid window hook: '"+name+"'");
  let arr = winhooks[name];
  if (arr === undefined) {
    arr = [];
    winhooks[name] = arr;
  }
  for (let h of arr) if (h === cb) return;
  arr.push(cb);
}


function imagoCallWindowHook (name, win) {
  //win.setTimeout(function() conlog("****", Cc["@mozilla.org/xre/app-info;1"].getService(Ci.nsIXULAppInfo).name), 3000); // "Pale Moon"
  if (typeof(name) !== "string" || !name) return;
  let arr = winhooks[name];
  if (arr !== undefined) {
    for (let h of arr) {
      try {
        h(win, name);
      } catch (e) {
        Component.utils.reportError(e);
      }
    }
  }
}


//////////////////////////////////////////////////////////////////////////////
let iosvc = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);


//////////////////////////////////////////////////////////////////////////////
// null or dom window
function getDomWindowForChannel (chan) {
  try {
    let notifyCBs = null;
    if (chan.notificationCallbacks) {
      notifyCBs = chan.notificationCallbacks;
    } else if (chan.loadGroup) {
      notifyCBs = chan.loadGroup.notifyCBs;
    }
    return (notifyCBs ? notifyCBs.getInterface(Ci.nsIDOMWindow) : null);
  } catch (e) {
    //userChrome.logError(e);
    return null;
  }
}


const ImageByteCounterProp = " _k8_imago_total ";

function getByteCounterForDomWindow (dw) {
  if (!dw) return 0;
  let doc = dw.document;
  if (typeof(doc) === "undefined") return 0;
  if (typeof(doc[ImageByteCounterProp]) !== "number") return 0;
  return doc[ImageByteCounterProp];
}

function setByteCounterForDomWindow (dw, count) {
  if (!dw || typeof(count) !== "number") return;
  let doc = dw.document;
  if (typeof(doc) === "undefined") return;
  doc[ImageByteCounterProp] = count;
}


function getBrowserForDomWindow (dw) {
  if (!dw) return null;
  if (!gBrowser) return null;
  if (typeof(gBrowser.getBrowserForDocument) !== "function") return null;
  return gBrowser.getBrowserForDocument(dw.top.document);
}


//////////////////////////////////////////////////////////////////////////////
function createInputStreamFromString (data) {
  if (typeof(data) !== "string") data = "";
  let ost = Cc["@mozilla.org/binaryoutputstream;1"].createInstance(Ci.nsIBinaryOutputStream);
  let sst = Cc["@mozilla.org/storagestream;1"].createInstance(Ci.nsIStorageStream);
  sst.init(8192, data.length, null); // default allocator
  ost.setOutputStream(sst.getOutputStream(0));
  ost.writeBytes(data, data.length);
  return sst.newInputStream(0);
}


//////////////////////////////////////////////////////////////////////////////
function url2uri (url) {
  return iosvc.newURI(url, null, null);
}


// only host and path
function url2HashKey (url) {
  if (typeof(url) === "string") url = iosvc.newURI(url, null, null);
  let pt = url.path;
  if (!pt || typeof(pt) !== "string") pt = "/";
  for (;;) {
    let hidx = pt.indexOf("#");
    if (hidx >= 0) { pt = pt.substring(0, hidx); continue; }
    hidx = pt.indexOf("?");
    if (hidx >= 0) { pt = pt.substring(0, hidx); continue; }
    break;
  }
  // this can pappen for "chrome:", for exampe
  return (url.host ? url.host : "nohost")+pt;
}


function cssEscape (s) s.replace(/([\]\[(){}"'`#.\/\\])/g, "\\$1"); //"


//////////////////////////////////////////////////////////////////////////////
// returns timer object
function createTimer (aCallback, aDelay, oneshot) {
  // create the timer object
  let timer = Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer);
  // The timer object may be garbage collected before it fires, so we need to
  // keep a reference to it alive (https://bugzil.la/647998).
  // However, simply creating a closure over the timer object without using it
  // may not be enough, as the timer might get optimized out of the closure
  // scope (https://bugzil.la/640629#c9). To work around this, the timer object
  // is explicitly stored as a property of the observer.
  let fired = false;
  var observer = {
    observe: function () {
      fired = true;
      // just-in-case check
      if (observer.timer && oneshot) delete observer.timer;
      aCallback();
    },
    timer: timer,
  };
  timer.init(observer, aDelay, (oneshot ? Ci.nsITimer.TYPE_ONE_SHOT : Ci.nsITimer.TYPE_REPEATING_SLACK));
  return {
    get active () !fired,
    get callback () aCallback,
    get oneShot () !!oneShot,
    get interval () aDelay,
    cancel: function () {
      if (observer.timer) {
        observer.timer.cancel();
        delete observer.timer;
      }
    },
  };
}

function oneShotTimer (aCallback, aDelay) createTimer(aCallback, aDelay, true);
function intervalTimer (aCallback, aDelay) createTimer(aCallback, aDelay, false);


//////////////////////////////////////////////////////////////////////////////
function getRulesIFile () {
  let pd = Cc["@mozilla.org/file/directory_service;1"].getService(Ci.nsIProperties).get("ProfD", Ci.nsIFile);
  pd.append("k8imago");
  pd.append("rules.rc");
  return pd;
}


//////////////////////////////////////////////////////////////////////////////
function getFileTextContents (file) {
  let encoding = "UTF-8";
  function toUnicode (text) {
    if (typeof(text) === "string") {
      if (text.length == 0) return "";
    } else if (text instanceof ArrayBuffer) {
      if (text.byteLength == 0) return "";
      text = new Uint8Array(text);
      return new TextDecoder(encoding).decode(text);
    } else if ((text instanceof Uint8Array) || (text instanceof Int8Array)) {
      if (text.length == 0) return "";
      return new TextDecoder(encoding).decode(text);
    } else {
      return "";
    }
    let converter = Cc["@mozilla.org/intl/scriptableunicodeconverter"].createInstance(Ci.nsIScriptableUnicodeConverter);
    converter.charset = encoding;
    let res = converter.ConvertToUnicode(text);
    if (res.length >= 3 && res.substr(0, 3) == "\u00EF\u00BB\u00BF") res = res.substr(3); // fuck BOM
    return res;
  }

  //debuglog("file=[", file.path, "]");
  // read file
  let inputStream = Cc["@mozilla.org/network/file-input-stream;1"].createInstance(Ci.nsIFileInputStream);
  inputStream.init(file, -1, -1, null);
  let scInputStream = Cc["@mozilla.org/scriptableinputstream;1"].createInstance(Ci.nsIScriptableInputStream);
  scInputStream.init(inputStream);
  let output = scInputStream.read(-1);
  scInputStream.close();
  inputStream.close();
  return toUnicode(output, encoding);
}
