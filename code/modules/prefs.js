/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
// preferences engine
//////////////////////////////////////////////////////////////////////////////
EXPORTED_SYMBOLS = [
  "PREFS",
  "BLACK_HOSTS",
  "WHITE_HOSTS",
  "registerPrefHook"
];

//////////////////////////////////////////////////////////////////////////////
let {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;


//////////////////////////////////////////////////////////////////////////////
Cu.import("resource://gre/modules/Services.jsm");

Cu.import("chrome://k8-imago-code/content/modules/debuglog.js");


//////////////////////////////////////////////////////////////////////////////
const PREFS_BRANCH = "extensions.k8imago.";
const PREFS_BRANCH_HOSTS = PREFS_BRANCH+"hosts.";

var PREFS = {};
var BLACK_HOSTS = {};
var WHITE_HOSTS = {};


//////////////////////////////////////////////////////////////////////////////
const hostTypeRE = /^hosts\.(black|white)\.(.+)$/;

function loadPrefs () {
  const hostRE = /^hosts\./;
  let branch = Services.prefs.getBranch(PREFS_BRANCH);
  let list = branch.getChildList("", {});
  for (let name of list) {
    if (hostRE.test(name)) {
      let mt = name.match(hostTypeRE);
      if (!mt) continue;
      if (branch.getPrefType(name) !== branch.PREF_BOOL) continue;
      let v = branch.getBoolPref(name);
      let host = mt[2];
      switch (mt[1]) {
        case "black": BLACK_HOSTS[host] = v; break;
        case "white": WHITE_HOSTS[host] = v; break;
      }
      continue;
    }
    let names = name.split(".");
    let pname = names[names.length-1];
    let curO = PREFS;
    for (let n of names.slice(0, -1)) {
      if (!n) continue;
      let no = curO[n];
      if (no === undefined) {
        no = {};
        curO[n] = no;
      }
      curO = no;
    }
    switch (branch.getPrefType(name)) {
      case branch.PREF_STRING: curO[pname] = branch.getCharPref(name); break;
      case branch.PREF_INT: curO[pname] = branch.getIntPref(name); break;
      case branch.PREF_BOOL: curO[pname] = branch.getBoolPref(name); break;
    }
  }
  //for (let [n, v] in Iterator(PREFS)) conlog("[", n, "]=(", typeof(v), ")[", v, "]");
  if ("debugLog" in PREFS) setDebugLog(PREFS.debugLog);
}


////////////////////////////////////////////////////////////////////////////////
// string key -- pref name
// array value -- list of hooks for this key
let prefHooks = {};


let prefObserver = {
  observe: function (aSubject, aTopic, aData) {
    let name = aData.substr(PREFS_BRANCH.length);
    // hosts
    let mt = name.match(hostTypeRE);
    if (mt) {
      let branch = Services.prefs.getBranch(PREFS_BRANCH);
      try {
        let v = branch.getBoolPref(name);
        let host = mt[2];
        //conlog("<", mt[1], "> changed (", host, "): new value: '", v, "'");
        switch (mt[1]) {
          case "black": BLACK_HOSTS[host] = v; break;
          case "white": WHITE_HOSTS[host] = v; break;
        }
      } catch (e) {
        //conlog("<", mt[1], "> killed (", host, ")");
        switch (mt[1]) {
          case "black": delete BLACK_HOSTS[host]; break;
          case "white": delete WHITE_HOSTS[host]; break;
        }
      }
      return;
    }
    // others
    if (name in PREFS) {
      let branch = Services.prefs.getBranch(PREFS_BRANCH);

      let names = name.split(".");
      let pname = names[names.length-1];
      let curO = PREFS;
      for (let n of names.slice(0, -1)) {
        if (!n) continue;
        let no = curO[n];
        if (no === undefined) {
          no = {};
          curO[n] = no;
        }
        curO = no;
      }

      let v = curO[pname];
      let ov = v;
      try {
        switch (typeof(v)) {
          case "boolean": v = branch.getBoolPref(name); break;
          case "number": v = branch.getIntPref(name); break;
          case "string": v = branch.getCharPref(name); break;
          default: return;
        }
        if (name === "debugLog") setDebugLog(v);
        curO[pname] = v;
        let hl = prefHooks[name];
        if (hl !== undefined) {
          for (let hook of hl) {
            if (typeof(hook) === "function") {
              try { hook(name, v, ov); } catch (e) {}
            }
          }
        }
      } catch (e) {}
    }
  },
};


function registerPrefHook (name, cb) {
  if (!(name in prefHooks)) prefHooks[name] = [];
  let hl = prefHooks[name];
  for (let hook of hl) if (hook === cb) return;
  hl.push(cb);
}


////////////////////////////////////////////////////////////////////////////////
loadPrefs();

Services.prefs.addObserver(PREFS_BRANCH, prefObserver, false);
