/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
// black&white lists
var EXPORTED_SYMBOLS = [
  "blockImage",
  "unblockImage",
  "isBlockedImage",
  "isUnblockedImage",
  "resetImageStatus",
  "imageLoaderActivated"
];

let {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;


//////////////////////////////////////////////////////////////////////////////
Cu.import("chrome://k8-imago-code/content/modules/utils.js");
Cu.import(MODULE_PATH+"debuglog.js");
Cu.import(MODULE_PATH+"prefs.js");


//////////////////////////////////////////////////////////////////////////////
function uri2key (uri, docuri) {
  let res = url2HashKey(uri);
  if (docuri) res += "|"+url2HashKey(docuri);
  return res;
}


//////////////////////////////////////////////////////////////////////////////
// format:
//  key string url  ; without '?' and '#'; docuri (also stripped) appended as "|"+docuri
//  value: object
//    int expireTime (-666: haven't seen yet)
//    int setTime (so we can remove stale unblocks)
//    bool blocked
let imgBlockList = {};
let clearTId = null;


// returns cleaned list
function cleanList (snow, list) {
  //let snow = Math.floor(Date.now()/1000);
  let badkeys = null;
  for (let [n, v] of Iterator(list)) {
    if (v.expireTime == -666) {
      if (snow-v.setTime < 10*60) continue;
    } else {
      if (v.expireTime > snow) continue;
    }
    if (!badkeys) badkeys = [];
    badkeys.push(n);
    if (PREFS.debugLog) conlog("expired: ", n);
  }
  if (badkeys) for (let n of badkeys) delete list[n];
  return list;
}


//////////////////////////////////////////////////////////////////////////////
function findMinSec (curmin, snow, list) {
  let mt = false;
  for (let [n, v] of Iterator(list)) {
    if (v.expireTime == -666) continue;
    if (mt === false || v.expireTime < mt) mt = v.expireTime;
  }
  if (mt !== false) {
    if (mt < snow) mt = snow;
    mt -= snow;
    if (curmin === false || mt < curmin) curmin = mt;
  }
  return curmin;
}


function minExpireTimeSec (snow) {
  let res = findMinSec(false, snow, imgBlockList);
  return res;
}


//////////////////////////////////////////////////////////////////////////////
function doCleanup () {
  clearTId = null;
  setCleanupTimer();
  if (PREFS.debugLog) conlog("cleanup timer fired!");
}


function setCleanupTimer () {
  if (clearTId === null) {
    let snow = Math.floor(Date.now()/1000);
    imgBlockList = cleanList(snow, imgBlockList);
    let secs = minExpireTimeSec(snow);
    if (secs === false) return;
    clearTId = oneShotTimer(doCleanup, secs*1000+500);
  }
}


function stoplistResetTimer () {
  if (clearTId) {
    clearTId.cancel();
    clearTId = null;
  }
  setCleanupTimer();
}


registerPrefHook("forgetSeconds", stoplistResetTimer);


//////////////////////////////////////////////////////////////////////////////
function blockImage (uri, docuri) {
  let url = uri2key(uri, docuri);
  //imgBlockList[url] = snow+PREFS.forgetSeconds;
  let snow = Math.floor(Date.now()/1000);
  imgBlockList[url] = {setTime:snow, expireTime:-666, blocked:true};
  stoplistResetTimer();
}


function unblockImage (uri, docuri, forceValidTime) {
  let url = uri2key(uri, docuri);
  //if (PREFS.debugLog) conlog("unblocking: ["+url+"]");
  //unblockedImages[url] = snow+PREFS.forgetSeconds;
  let snow = Math.floor(Date.now()/1000);
  let etime = (forceValidTime ? snow+PREFS.forgetSeconds : -666);
  imgBlockList[url] = {setTime:snow, expireTime:etime, blocked:false};
  stoplistResetTimer();
}


function findImageBlockInfo (uri, docuri) {
  let url = uri2key(uri, docuri);
  if (url in imgBlockList) {
    let it = imgBlockList[url];
    let snow = Math.floor(Date.now()/1000);
    if ((it.expireTime == -666 && snow-it.setTime >= 10*60) ||
        (it.expireTime != -666 && snow-it.expireTime > PREFS.forgetSeconds))
    {
      delete imgBlockList[url];
      stoplistResetTimer();
      return null;
    }
    return it;
  }
  return null;
}


function isBlockedImage (uri, docuri) {
  let it = findImageBlockInfo(uri, docuri);
  return (it && it.blocked);
}


function isUnblockedImage (uri, docuri) {
  let it = findImageBlockInfo(uri, docuri);
  return (it && !it.blocked);
}


function resetImageStatus (uri, docuri) {
  let url = uri2key(uri, docuri);
  if (url in imgBlockList) {
    if (PREFS.debugLog) conlog("resetting image status for ["+url+"]");
    delete imgBlockList[url];
    stoplistResetTimer();
  }
}


function imageLoaderActivated (uri, docuri) {
  let it = findImageBlockInfo(uri, docuri);
  if (it && it.expireTime == -666) {
    if (PREFS.debugLog) conlog("setting valid block timeout for ["+uri2key(uri, docuri)+"]");
    it.expireTime = Math.floor(Date.now()/1000)+PREFS.forgetSeconds;
  }
}
