/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
// http observer
var EXPORTED_SYMBOLS = [
];

let {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;


//////////////////////////////////////////////////////////////////////////////
Cu.import("resource://gre/modules/Services.jsm");

//////////////////////////////////////////////////////////////////////////////
Cu.import("chrome://k8-imago-code/content/modules/utils.js");
Cu.import(MODULE_PATH+"debuglog.js");
Cu.import(MODULE_PATH+"prefs.js");
Cu.import(MODULE_PATH+"tamper.js");
Cu.import(MODULE_PATH+"stoplist.js");
Cu.import(MODULE_PATH+"imgreload.js");
Cu.import(MODULE_PATH+"hazard.js");
Cu.import(MODULE_PATH+"rule-engine.js");


//////////////////////////////////////////////////////////////////////////////
// init some functions from window
let btoa = null;

registerWindowHook("load", function (win) {
  btoa = win.btoa;
});

registerWindowHook("unload", function (win) {
  btoa = null;
});


//////////////////////////////////////////////////////////////////////////////
let tldsvc = Cc["@mozilla.org/network/effective-tld-service;1"].getService(Ci.nsIEffectiveTLDService);


//////////////////////////////////////////////////////////////////////////////
let imagectRE = /^image\//;


let httpRequestObserver = {
  QueryInterface: function (aIID) {
    if (aIID.equals(Ci.nsIObserver) || aIID.equals(Ci.nsISupports)) return this;
    throw Components.results.NS_NOINTERFACE;
  },

  observe: function (aSubject, aTopic, aData) {
    if (!(aSubject instanceof Ci.nsIHttpChannel)) return;

    if (aTopic === "http-on-examine-response") {
      // aSubject is request object
      let httpChannel = aSubject.QueryInterface(Ci.nsIHttpChannel);

      let dw = getDomWindowForChannel(httpChannel), bro = null;
      if (dw) {
        bro = getBrowserForDomWindow(dw);
        if (bro) {
          if (PREFS.debugLog) conlog("[", httpChannel.URI.spec, "] browser: ", bro.currentURI.spec);
        } else {
          if (PREFS.debugLog) conlog("*** [", httpChannel.URI.spec, "] NO browser");
          return;
        }
      } else {
        if (PREFS.debugLog) conlog("*** [", httpChannel.URI.spec, "] NO dom window");
        return;
      }
      let docURI = bro.currentURI.spec;

      // process redirects for whitelisted and blacklisted images
      if (httpChannel.responseStatus >= 300 && httpChannel.responseStatus <= 399) {
        let wl = isUnblockedImage(httpChannel.URI, docURI);
        let bl = isBlockedImage(httpChannel.URI, docURI);
        if (wl || bl) {
          imageLoaderActivated(httpChannel.URI, docURI);
          if (PREFS.debugLog) conlog("***GOT IMAGE REDIRECT FOR ["+httpChannel.URI.spec+"]! responseStatus="+httpChannel.responseStatus);
          let loc = httpChannel.getResponseHeader("Location");
          if (loc) {
            if (PREFS.debugLog) conlog("***REDIRECT TO: ["+loc+"]");
            if (wl) {
              unblockImage(loc, docURI);
              registerManualReloadRedirectURL(httpChannel.URI.spec, loc);
            } else {
              blockImage(loc, docURI);
            }
          }
        }
      }

      let ctype = httpChannel.contentType;
      if (!ctype) return;
      if (!imagectRE.test(ctype) /*&& ctype !== "application/octet-stream"*/) {
        // if we got a non-redirecting reply, reset image status
        resetImageStatus(httpChannel.URI, docURI);
        //conlog("ctype: ", ctype);
        return;
      }

      // intercept and count
      if (Math.floor(httpChannel.responseStatus/100) === 2) {
        imageLoaderActivated(httpChannel.URI, docURI);
        let doSizeBlockInTamper = false;

        if (PREFS.debugLog) conlog("*** CHECKING [", httpChannel.URI.spec, "]...");
        let isoctet = false/*ctype === "application/octet-stream"*/;
        // whitelisted?
        if (isUnblockedImage(httpChannel.URI, docURI)) {
          if (PREFS.debugLog) conlog("*** UNBLOCKED [", httpChannel.URI.spec, "]...");
          return;
        }
        // blacklisted?
        let blacklisted = (isBlockedImage(httpChannel.URI, docURI));
        // global black/white lists
        if (!blacklisted) {
          //TODO: list priorities
          // this can pappen for "chrome:", for exampe
          let host = (httpChannel.URI.host ? httpChannel.URI.host : "nohost");
          if (WHITE_HOSTS[host]) {
            // whitelisted
            if (PREFS.debugLog) conlog("*** WHITE HOST [", httpChannel.URI.spec, "]...");
            return;
          }
          if (host in BLACK_HOSTS) blacklisted = BLACK_HOSTS[host];
        }
        if (!blacklisted && dw && PREFS.maxTotalBytesForDoc > 0) {
          try {
            let ctlen = parseInt(httpChannel.getResponseHeader("Content-Length"), 10);
            if (isNaN(ctlen)) doSizeBlockInTamper = true;
            if (!isNaN(ctlen) && ctlen > 0) {
              let isize = getByteCounterForDomWindow(dw)+ctlen;
              if (isize >= PREFS.maxTotalBytesForDoc) {
                if (PREFS.debugLog) conlog("*** [", httpChannel.URI.spec, "]: blacklisted due to bytes limit: isize="+isize+"; max="+PREFS.maxTotalBytesForDoc+"; ctlen="+ctlen);
                blacklisted = true;
              } else {
                setByteCounterForDomWindow(dw, isize);
              }
            }
          } catch (e) {
            // httpChannel.getResponseHeader can throw when download is blocked (like font), just get out of here
            return;
          }
        }
        // check rules
        let rule = null;
        if (!blacklisted) {
          // check if we have too much images for this DOM window
          if (PREFS.debugLog) conlog("*** [", httpChannel.URI.spec, "] searching for rule...");
          rule = getRuleForURI(httpChannel.URI, bro.currentURI);
          if (rule) {
            if (PREFS.debugLog) conlog("*** [", httpChannel.URI.spec, "] rule: ", rule);
            if (rule.stopAction) {
              if (rule.action === "deny") {
                if (PREFS.debugLog) conlog("*** [", httpChannel.URI.spec, "] blocked by 'deny' rule!");
                blacklisted = true;
              }
            }
          }
        }
        //conlog("bl: [", httpChannel.URI.spec, "]: ", blacklisted);
        // check for 1st-party images
        if (!blacklisted && PREFS.allowFirstPartyImages && !bro.currentURI.equals(httpChannel.URI)) {
          // i've seen "Error: NS_ERROR_FAILURE: Component returned failure code: 0x80004005 (NS_ERROR_FAILURE) [nsIURI.host]" there, so...
          try {
            if (tldsvc.getBaseDomain(bro.currentURI) === tldsvc.getBaseDomain(httpChannel.URI)) {
              if (PREFS.debugLog) conlog(httpChannel.URI.spec, ": first-party!");
              return;
            }
          } catch (e) {}
        }
        //if (PREFS.debugLog) conlog("counting: ", httpChannel.URI.spec);
        let nlst = new ImgDetectListener(isoctet, blacklisted, doSizeBlockInTamper);
        nlst.rule = rule;
        nlst.mainURI = bro.currentURI;
        aSubject.QueryInterface(Ci.nsITraceableChannel);
        nlst.olst = aSubject.setNewListener(nlst);
        nlst.inited();
        // onStartRequest() and onStopRequest() will be called anyway, and
        // our tamper will process that correctly for blacklisted images
        if (blacklisted) aSubject.cancel(Cr.NS_BINDING_ABORTED);
      } else {
        if (PREFS.debugLog) conlog("WTF: ", httpChannel.URI.spec, " (", httpChannel.responseStatus, ")");
        //aSubject.cancel(Cr.NS_BINDING_ABORTED);
        resetImageStatus(httpChannel.URI, docURI);
      }
    } else if (aTopic === "http-on-modify-request") {
      let httpChannel = aSubject.QueryInterface(Ci.nsIHttpChannel);
      let url = httpChannel.URI.spec;
      // WARNING! `isManualReloadURL()` modifies internal URL cache (i.e. it is destructive!)
      if (isManualReloadURL(url)) {
        // ah, ok, i took time from rfc
        httpChannel.setRequestHeader("If-Modified-Since", "Sat, 29 Oct 1994 19:43:31 GMT", false);
        httpChannel.setRequestHeader("ETag", "", false);
        //delete reloadUrlList[url]; // no need to, `isManualReloadURL()` will do it for us
      }
    }
  },
};


//////////////////////////////////////////////////////////////////////////////
let osvc = Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService);
osvc.addObserver(httpRequestObserver, "http-on-examine-response", false);
osvc.addObserver(httpRequestObserver, "http-on-modify-request", false);
