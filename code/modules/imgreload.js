/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
// reload image
var EXPORTED_SYMBOLS = [
  "reloadImage",
  "isManualReloadURL",
  "registerManualReloadRedirectURL",
  "findAnyBlockedImage"
];

let {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;


//////////////////////////////////////////////////////////////////////////////
Cu.import("chrome://k8-imago-code/content/modules/utils.js");
Cu.import(MODULE_PATH+"debuglog.js");
Cu.import(MODULE_PATH+"prefs.js");
Cu.import(MODULE_PATH+"stoplist.js");

//let iosvc = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);


//////////////////////////////////////////////////////////////////////////////
/*
function expungeImageCache (el, url) {
  if (!el || typeof(el.tagName) !== "string" || el.tagName !== "IMG") {
    if (PREFS.debugLog) conlog("WTF! ["+url+"] is not an image!");
    return;
  }
  try {
    //if (PREFS.debugLog) conlog(" converting URL=[", url, "]...");
    //let ourl = url;
    //let uri = iosvc.newURI(url, null, null);
    //let url = uri.spec;
    //if (PREFS.debugLog) conlog(" URL=[", ourl, "]; URI=[", url, "]");
    let tools = Cc["@mozilla.org/image/tools;1"].getService(Ci.imgITools);
    let cache = tools.getImgCacheForDocument(el.ownerDocument);
    if (cache) {
      if (url === undefined) {
        url = el.getAttribute("src");
        if (PREFS.debugLog) conlog(" got URL from image: URL=[", url, "]...");
      }
      if (typeof(url) === "string") {
        if (PREFS.debugLog) conlog(" converting URL=[", url, "] to URI");
        let baseURI = null;
        try {
          if (PREFS.debugLog) conlog(" docURI=[", el.ownerDocument.documentURI, "]");
          baseURI = iosvc.newURI(el.ownerDocument.documentURI, null, null);
          if (PREFS.debugLog) conlog(" BASE=[", baseURI.spec, "]");
        } catch (e) {}
        url = iosvc.newURI(url, null, baseURI);
      }
      if (PREFS.debugLog) conlog("got cache object for image with url ", url.spec);
      try {
        // shitzilla assmonkeys are fuckin' shitheads
        //cache.removeEntry(url);
        cache.clearCache(false); // evict everything except chrome images.
      } catch (e) {
        if (PREFS.debugLog) conlog(" error clearing cache for image with url ", url.spec);
        if (PREFS.debugLog) logException("cache error", e);
      }
    } else {
      if (PREFS.debugLog) conlog("CAN'T get cache object for document");
    }
  } catch (e) {
    if (PREFS.debugLog) conlog(" some error occuren when clearing cache for image with url ", el.src);
    if (PREFS.debugLog) logException("unknown cache error", e);
  }
}
*/


//////////////////////////////////////////////////////////////////////////////
// shitzilla assmonkeys are fuckin' shitheads removed the way to kill only one image in cache
// so we resort to clearing the whole image cache. fuck you, shitzilla
// so `url` is used only for error messages
function expungeImageCache (el, url) {
  if (!url) url = "<wutafuck>";
  if (!el || typeof(el.tagName) !== "string" || el.tagName !== "IMG") {
    if (PREFS.debugLog) conlog("WTF! ["+url+"] is not an image!");
    return;
  }
  try {
    let bb = el.ownerDocument.documentElement;
    if (bb) {
      if (bb.getAttribute("k8imago-cache-cleared") === "tan") {
        conlog("image cache already cleared");
        return;
      }
      bb.setAttribute("k8imago-cache-cleared", "tan");
    }
    let tools = Cc["@mozilla.org/image/tools;1"].getService(Ci.imgITools);
    let cache = tools.getImgCacheForDocument(el.ownerDocument);
    if (cache) {
      if (PREFS.debugLog) conlog("got cache object for image with url ["+url+"]");
      try {
        cache.clearCache(false); // evict everything except chrome images.
      } catch (e) {
        if (PREFS.debugLog) conlog(" error clearing cache for image with url ["+url+"]");
        if (PREFS.debugLog) logException("cache error", e);
      }
    } else {
      if (PREFS.debugLog) conlog("CAN'T get cache object for document");
    }
  } catch (e) {
    if (PREFS.debugLog) conlog(" some error occuren when clearing cache for image with url ["+url+"]");
    if (PREFS.debugLog) logException("unknown cache error", e);
  }
}


//////////////////////////////////////////////////////////////////////////////
/*
  key: full url string
  value: object
    int count -- how many times this url was queried
    double lastQueryTime -- from `Date.now()`
 */
let reloadUrlList = {};


// WARNING! `isManualReloadURL()` modifies internal URL cache (i.e. it is destructive!)
function isManualReloadURL (url) {
  if (typeof(url) !== "string" || !url) return false;
  let ctt = Date.now();
  if (url in reloadUrlList) {
    let qo = reloadUrlList[url];
    if (qo.count < 1 || ctt-qo.lastQueryTime > 13000) {
      delete reloadUrlList[url];
      return false;
    }
    qo.count -= 1;
    if (qo.count == 0) delete reloadUrlList[url];
    return true;
  } else {
    return false;
  }
}


function registerReloadURL (url) {
  if (typeof(url) !== "string" || !url) return;
  let ctt = Date.now();
  if (url in reloadUrlList) {
    let qo = reloadUrlList[url];
    if (qo.count && ctt-qo.lastQueryTime > 13000) qo.count = 0;
    qo.count += 1;
    qo.lastQueryTime = ctt;
  } else {
    reloadUrlList[url] = {count:1, lastQueryTime:ctt};
  }
}


function registerManualReloadRedirectURL (oldurl, newurl) {
  if (typeof(oldurl) !== "string" || !oldurl) return;
  if (typeof(newurl) !== "string" || !newurl) return;
  if (oldurl == newurl) return;
  if (newurl in reloadUrlList) return;
  if (oldurl in reloadUrlList) {
    let qo = reloadUrlList[oldurl];
    reloadUrlList[newurl] = qo;
  }
}


function findAnyBlockedImage (doc) {
  if (!doc || !doc.images || !doc.images.length) return null;
  for (let iidx = 0; iidx < doc.images.length; ++iidx) {
    let ii = doc.images[iidx];
    if (!(ii instanceof Ci.nsIImageLoadingContent)) {
      //conlog("*** IMAGE #"+iidx+" is not an `nsIImageLoadingContent`");
      continue;
    }
    let src = ii.src;
    if (!src) {
      //conlog("*** IMAGE #"+iidx+" has empty src");
      continue;
    }
    if (ii.getAttribute("k8imago-reloaded") === "tan") {
      //conlog("*** IMAGE #"+iidx+" is already reloaded; src=["+src+"]");
      continue;
    }
    if (ii.getAttribute("k8imago-mark") !== "tan") {
      // sadly, blocked images will go to the cache; try some heuristic
      if (ii.naturalWidth == 48 && ii.naturalHeight == 48 && ii.width != 48 && ii.height != 48) {
        //conlog("*** IMAGE #"+iidx+" seems to be blocked w/o the flag; src=["+src+"]");
      } else {
        //conlog("*** IMAGE #"+iidx+" is not blocked; src=["+src+"]");
        continue;
      }
    }
    //conlog("*** IMAGE #"+iidx+" is a valid candidate; src=["+src+"]");
    return ii;
  }
  return null;
}


function imgRestoreAttrs (el) {
  if (!el || !el.getAttribute || !el.setAttribute || !el.removeAttribute) return;

  let title = el.getAttribute("k8imago-saved-title");
  if (typeof(title) === "string") {
    el.setAttribute("title", title);
    el.removeAttribute("k8imago-saved-title");
  }

  let alt = el.getAttribute("k8imago-saved-alt");
  if (typeof(alt) === "string") {
    el.setAttribute("alt", alt);
    el.removeAttribute("k8imago-saved-alt");
  }
}


var reloadImage = null;


function ImgBlinker (obs) {
  if (!obs.k8Active) return;
  if (!obs.k8BlinkTimer) return;
  let img = obs.k8img;
  if (!img) {
    obs.k8BlinkTimer.cancel();
    delete obs.k8BlinkTimer;
  }
  obs.k8BlinkPhase = 1-obs.k8BlinkPhase;
  img.style.opacity = (obs.k8BlinkPhase ? 0.6 : 0.4);
}


function ImgScriptedObs () {
  this.k8RestoreImg = function () {
    if (!this.k8Active) return;
    if (!this.k8img) return;
    if (this.k8BlinkTimer) {
      this.k8BlinkTimer.cancel();
      delete this.k8BlinkTimer;
    }
    let el = this.k8img;
    delete this.k8img;
    // restore image properties
    if (PREFS.debugLog) conlog("*** restoring opacity for ["+el.src+"] ["+el.ownerDocument.documentURI+"] ("+el.ownerDocument.images.length+" images)");
    el.style.opacity = this.k8opacity;
    imgRestoreAttrs(el);
    //el.removeAttribute("k8imago-mark"); // this image is loaded

    try {
      // reload all other images with the same source URL unobserved
      if (PREFS.debugLog) conlog("*** scanning document ["+el.ownerDocument.documentURI+"] ("+el.ownerDocument.images.length+" images)");
      for (let iidx = 0; iidx < el.ownerDocument.images.length; ++iidx) {
        let ii = el.ownerDocument.images[iidx];
        if (ii == el) continue;
        if (!(ii instanceof Ci.nsIImageLoadingContent)) continue;
        if (ii.getAttribute("src") !== el.src) continue;
        if (ii.getAttribute("k8imago-reloaded") === "tan") continue;
        if (ii.getAttribute("k8imago-mark") !== "tan") continue; // not blocked
        ii.setAttribute("k8imago-reloaded", "tan");
        ii.forceReload();
        imgRestoreAttrs(ii);
      }
    } catch (e) {
      if (PREFS.debugLog) logException("scan error", e);
    }

    // check if we need to force-reload next image
    if (!this.k8AllReloads) return;
    if (PREFS.debugLog) conlog("*** all-reload mode active");
    // yeah, let's do it
    //let docURI = el.ownerDocument.documentURI;
    let nextimg = findAnyBlockedImage(el.ownerDocument);
    if (!nextimg) {
      if (PREFS.debugLog) conlog("*** OOPS, NO IMAGES!");
      return;
    }
    if (PREFS.debugLog) conlog("*** scheduling reload for ["+nextimg.src+"]");
    oneShotTimer(function () { reloadImage(nextimg, true); }, 1500);
  };
  this.k8DebugEnabled = PREFS.debugLog;
  this.k8Debug = function (eventname) {
    if (!this.k8DebugEnabled) return;
    if (!this.k8Active) return;
    conlog("*** EVENT <"+eventname+"> for ["+this.k8ImgSrc+"] ***");
  };
  this.sizeAvailable = function sizeAvailable (aRequest) { this.k8Debug("sizeAvailable"); };
  this.frameComplete = function frameComplete (aRequest) { this.k8Debug("frameComplete"); };
  this.decodeComplete = function decodeComplete (aRequest) { this.k8Debug("decodeComplete"); };
  this.loadComplete = function loadComplete (aRequest) { this.k8Debug("loadComplete"); this.k8RestoreImg(); };
  this.frameUpdate = function frameUpdate (aRequest) { this.k8Debug("frameUpdate"); };
  this.discard = function discard (aRequest) { this.k8Debug("discard"); };
  this.isAnimated = function isAnimated (aRequest) { this.k8Debug("isAnimated"); };
  this.hasTransparency = function hasTransparency (aRequest) { this.k8Debug("hasTransparency"); };
}


reloadImage = function (el, allReloads) {
  if (el instanceof Ci.nsIImageLoadingContent) {
    if (PREFS.debugLog) conlog("forcing reload!");
    let src = el.getAttribute("src");
    if (!src) return;

    el.setAttribute("k8imago-reloaded", "tan");

    if (PREFS.debugLog) conlog("*** SRC=["+src+"]; type=["+typeof(src)+"]");
    let bb = el.ownerDocument.documentElement;
    if (allReloads) {
      let docURI = el.ownerDocument.documentURI;
      if (PREFS.debugLog) conlog("*** UNBLOCKING: SRC=["+src+"] ["+docURI+"]");
      unblockImage(src, docURI);
    }

    registerReloadURL(src);
    expungeImageCache(el, src);
    //oneShotTimer(function () { delete reloadUrlList[src]; }, 500);
    try {
      if (PREFS.debugLog) conlog("...creating observer...");
      let obs = new ImgScriptedObs();
      obs.k8ImgSrc = src;
      //obs.k8docURI = el.ownerDocument.documentURI;
      obs.k8img = el;
      obs.k8opacity = el.style.opacity;
      if (allReloads) obs.k8AllReloads = true;
      obs.k8Active = false;

      if (PREFS.debugLog) conlog("...creating scripted observer...");
      let sobs = Cc["@mozilla.org/image/tools;1"].getService(Ci.imgITools).createScriptedObserver(obs);
      if (PREFS.debugLog) conlog("...setting scripted observer...");
      el.addObserver(sobs);

      if (PREFS.debugLog) conlog("...forcing reload...");
      el.style.opacity = 0.4;
      obs.k8BlinkPhase = 0;
      obs.k8BlinkTimer = intervalTimer(function () { ImgBlinker(obs); }, 333);
      obs.k8Active = true;
      el.forceReload();
    } catch (e) {
      if (PREFS.debugLog) logException("reload error", e);
    }
  } else {
    if (PREFS.debugLog) conlog("alas...");
  }
};
