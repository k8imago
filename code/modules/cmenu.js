/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
// context menu
var EXPORTED_SYMBOLS = [
  "CMenu"
];

let {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;


//////////////////////////////////////////////////////////////////////////////
Cu.import("chrome://k8-imago-code/content/modules/utils.js");
Cu.import(MODULE_PATH+"debuglog.js");
Cu.import(MODULE_PATH+"prefs.js");
Cu.import(MODULE_PATH+"stoplist.js");
Cu.import(MODULE_PATH+"imgreload.js");


//////////////////////////////////////////////////////////////////////////////
// image context menu
function CMenu (win) {
  this.win = win;
  this.imgEl = null;

  let me = this;
  let menu = win.document.getElementById("contentAreaContextMenu");
  if (menu) {
    conlog("context menu found!");
    menu.addEventListener("popupshowing", function () me.contextPopupShowing(), false);
    menu.addEventListener("popuphiding", function () me.contextPopupHiding(), false);
  }
}


CMenu.prototype = {
  contextPopupShowing: function () {
    let me = this;
    let gcm = me.win.gContextMenu;
    let doc = me.win.document;

    function $ (name) doc.getElementById(name);

    function addMenuItem (id, label, cb) {
      let mi = $("context-viewimage");
      if (!mi) return null;
      let el = $(id);
      if (!el) {
        el = doc.createElement("menuitem");
        el.setAttribute("id", id);
        el.addEventListener("command", cb, false);
        mi.parentNode.insertBefore(el, mi);
      }
      if (label) el.setAttribute("label", label);
      el.hidden = false;
      return el;
    }

    function addMenuSeparator (id) {
      let mi = $("context-viewimage");
      if (!mi) return null;
      let el = $(id);
      if (!el) {
        el = doc.createElement("menuseparator");
        el.setAttribute("id", id);
        mi.parentNode.insertBefore(el, mi);
      }
      el.hidden = false;
      return el;
    }

    function hide (id) {
      let el = $(id);
      if (el) el.hidden = true;
    }

    function hideAll () {
      hide("k8-imago-separator0");
      hide("k8-imago-white-all");
      hide("k8-imago-separator1");
      hide("k8-imago-white");
      hide("k8-imago-black");
      hide("k8-imago-reset");
      hide("k8-imago-white-noreload");
      hide("k8-imago-reload");
      hide("k8-imago-separator2");
    }

    me.imgEl = null;

    if (gcm.onImage) {
      me.imgEl = gcm.target;
      if (!me.imgEl.getAttribute("src")) { me.imgEl = null; hideAll(); return; }

      addMenuSeparator("k8-imago-separator0");

      addMenuItem("k8-imago-white-all", "Whitelist all images ("+PREFS.forgetSeconds+" secs)", function () {
        if (!me.imgEl) return;
        //let docURI = me.imgEl.ownerDocument.documentURI;
        //for (let img of me.imgEl.ownerDocument.querySelectorAll("img"))
        if (!me.imgEl.ownerDocument.images) {
          if (PREFS.debugLog) conlog("*** OOPS, NO IMAGES!");
          return;
        }
        // force reloading all images
        reloadImage(me.imgEl, true);
      });

      addMenuSeparator("k8-imago-separator1");

      addMenuItem("k8-imago-white", "Whitelist image ("+PREFS.forgetSeconds+" secs)", function () {
        if (!me.imgEl) return;
        let docURI = me.imgEl.ownerDocument.documentURI;
        unblockImage(me.imgEl.src, docURI);
        reloadImage(me.imgEl);
      });

      addMenuItem("k8-imago-black", "Blacklist image ("+PREFS.forgetSeconds+" secs)", function () {
        if (!me.imgEl) return;
        let docURI = me.imgEl.ownerDocument.documentURI;
        blockImage(me.imgEl.src, docURI);
        reloadImage(me.imgEl);
      });

      addMenuItem("k8-imago-reset", "Reset image to default", function () {
        if (!me.imgEl) return;
        let docURI = me.imgEl.ownerDocument.documentURI;
        resetImageStatus(me.imgEl.src, docURI);
        reloadImage(me.imgEl);
      });

      addMenuItem("k8-imago-white-noreload", "Whitelist image w/o reload ("+PREFS.forgetSeconds+" secs)", function () {
        if (!me.imgEl) return;
        let docURI = me.imgEl.ownerDocument.documentURI;
        unblockImage(me.imgEl.src, docURI, true);
      });

      addMenuItem("k8-imago-reload", "Reload image", function () reloadImage(me.imgEl));

      addMenuSeparator("k8-imago-separator2");
    } else {
      hideAll();
    }
  },

  contextPopupHiding: function () {
    this.imgEl = null;
  },
};


//////////////////////////////////////////////////////////////////////////////
registerWindowHook("load", function (win) new CMenu(win));
