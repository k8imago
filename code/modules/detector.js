/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
// image parsing engine
var EXPORTED_SYMBOLS = [
  "FormatChecker"
];

let {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;


//////////////////////////////////////////////////////////////////////////////
function getU8 (data, ofs) (ofs >= 0 && ofs < data.length ? data.charCodeAt(ofs) : Number.NaN);

function getU16LE (data, ofs) (ofs >= 0 && ofs < data.length-1 ? data.charCodeAt(ofs)+0x100*data.charCodeAt(ofs+1) : Number.NaN);
function getU16BE (data, ofs) (ofs >= 0 && ofs < data.length-1 ? 0x100*data.charCodeAt(ofs)+data.charCodeAt(ofs+1) : Number.NaN);
function getU24LE (data, ofs) (ofs >= 0 && ofs < data.length-2 ? data.charCodeAt(ofs)+0x100*data.charCodeAt(ofs+1)+0x10000*data.charCodeAt(ofs+2) : Number.NaN);
function getU32LE (data, ofs) (ofs >= 0 && ofs < data.length-3 ? data.charCodeAt(ofs)+0x100*data.charCodeAt(ofs+1)+0x10000*data.charCodeAt(ofs+2)+0x1000000*data.charCodeAt(ofs+3) : Number.NaN);
function getU32BE (data, ofs) (ofs >= 0 && ofs < data.length-3 ? 0x1000000*data.charCodeAt(ofs)+0x10000*data.charCodeAt(ofs+1)+0x100*data.charCodeAt(ofs+2)+data.charCodeAt(ofs+3) : Number.NaN);

function getI16LE (data, ofs) {
  let v = getU16LE(data, ofs);
  if (!isNaN(v) && v > 0x7fff) v -= 0x10000;
  return v;
}

function getI16BE (data, ofs) {
  let v = getU16BE(data, ofs);
  if (!isNaN(v) && v > 0x7fff) v -= 0x10000;
  return v;
}

function getI32LE (data, ofs) {
  let v = getU32LE(data, ofs);
  if (!isNaN(v) && v > 0x7fffffff) v -= 0x100000000;
  return v;
}

function getI32BE (data, ofs) {
  let v = getU32BE(data, ofs);
  if (!isNaN(v) && v > 0x7fffffff) v -= 0x100000000;
  return v;
}


////////////////////////////////////////////////////////////////////////////////
const IMAGE_NEED_MORE_DATA = 0;
const IMAGE_ACCEPTED       = 1; // `detect()` accepted this image
const IMAGE_REJECTED       = 2; // `detect()` was wrong, this is not our format
const IMAGE_INVALID        = 3; // detected, invalid


////////////////////////////////////////////////////////////////////////////////
/*
 * string name -- format name
 * int minDetectBytes -- mininum bytes to detect
 * int minHeaderBytes -- mininum bytes to start parsing
 * int maxHeaderBytes -- maximum bytes to stop parsing
 * function detect (string data)
 *   will never be called with less than `minDetectBytes`
 *   must be fast (do more checks in `parse()` if necessary
 *   this is used to do "early reject"
 *   returns IMAGE_XXX
 * function parse (string data)
 *   parse should not be called on non-detected images
 *   will never be called with less than `minHeaderBytes`
 *   can be called with more than `maxHeaderBytes`
 *   returns: IMAGE_XXX or {int width, int height}
 */
let ImageFormats = [
  // jpeg image
  {
    name: "JPEG",
    minDetectBytes: 2,
    minHeaderBytes: 9,
    maxHeaderBytes: 65536,

    detect: function (data) {
      if (data[0] !== '\xff' || data[1] !== '\xd8') return IMAGE_REJECTED;
      return IMAGE_ACCEPTED;
    },

    parse: function (data) {
      // process chunks (segments)
      let pos = 2;
      while (pos+4 < data.length) {
        if (data[pos] !== '\xff') return IMAGE_INVALID;
        let segid = getU8(data, pos+1);
        if (isNaN(segid)) return IMAGE_NEED_MORE_DATA;
        pos += 2;
        let size = getU16BE(data, pos);
        if (isNaN(size)) return IMAGE_NEED_MORE_DATA;
        if (size < 2) return IMAGE_INVALID;
        // SOF?
        if (segid === 0xc0 || segid === 0xc2) {
          // bpp(byte), wdt(word), hgt(word)
          if (size < 5) return IMAGE_INVALID;
          if (pos+6 >= data.length) return IMAGE_NEED_MORE_DATA;
          return {
            width: getU16BE(data, pos+3),
            height: getU16BE(data, pos+5),
          };
        }
        pos += size;
      }
      return IMAGE_NEED_MORE_DATA;
    },
  },
  // png image
  {
    name: "PNG",
    minDetectBytes: 8,
    minHeaderBytes: 20,
    maxHeaderBytes: 65536,

    detect: function (data) {
      if (data[0] !== '\x89' || data[1] !== 'P' || data[2] !== 'N' || data[3] !== 'G') return IMAGE_REJECTED;
      if (data[4] !== '\x0d' || data[5] !== '\x0a') return IMAGE_REJECTED;
      if (data[6] !== '\x1a' || data[7] !== '\x0a') return IMAGE_REJECTED;
      return IMAGE_ACCEPTED;
    },

    parse: function (data) {
      // process chunks
      let pos = 8;
      while (pos < data.length) {
        let size = getU32BE(data, pos);
        if (isNaN(size)) return IMAGE_NEED_MORE_DATA;
        if (size > 65536) return IMAGE_INVALID; // invalid -- chunk too big
        if (pos+size+8 > data.length) return IMAGE_NEED_MORE_DATA;
        if (data[pos+4] === 'I' && data[pos+5] === 'H' && data[pos+6] === 'D' && data[pos+7] === 'R') {
          // header chunk, get size
          if (size < 8) return IMAGE_INVALID;
          return {
            width: getU32BE(data, pos+8),
            height: getU32BE(data, pos+12),
          };
        }
        // skip chunk
        pos += size+12; // size,name,data,crc
      }
      return IMAGE_NEED_MORE_DATA;
    },
  },
  // gif image
  {
    name: "GIF",
    minDetectBytes: 6,
    minHeaderBytes: 10,
    maxHeaderBytes: 10,

    detect: function (data) {
      if (data[0] !== 'G' || data[1] !== 'I' || data[2] !== 'F' || data[3] !== '8' || data[5] !== 'a') return IMAGE_REJECTED;
      if (data[4] !== '7' && data[4] !== '9') return IMAGE_REJECTED;
      return IMAGE_ACCEPTED;
    },

    parse: function (data) {
      return {
        width: getU16LE(data, 6),
        height: getU16LE(data, 8),
      };
    },
  },
  // shitdowz icon (some stupid sites still using that for favicons)
  {
    name: "ICO",
    minDetectBytes: 4,
    minHeaderBytes: 8,
    maxHeaderBytes: 8,

    detect: function (data) {
      if (data[0] !== '\x00' || data[1] !== '\x00') return IMAGE_REJECTED;
      if (data[2] !== '\x01' || data[3] !== '\x00') return IMAGE_REJECTED;
      return IMAGE_ACCEPTED;
    },

    parse: function (data) {
      // max 8 icons in set
      if (data[4] === '\x00' || data.charCodeAt(4) > 8 || data.charCodeAt(5) > 0) return IMAGE_INVALID;
      let res = {
        width: getU8(data, 6),
        height: getU8(data, 7),
      };
      if (res.width === 0) res.width = 256;
      if (res.height === 0) res.height = 256;
      return res;
    },
  },
  // shitdowz bitmap (no, really, some shitheads using that!)
  {
    name: "BMP",
    minDetectBytes: 2,
    minHeaderBytes: 26,
    maxHeaderBytes: 26,

    detect: function (data) {
      if (data[0] !== 'B' || data[1] !== 'M') return IMAGE_REJECTED;
      return IMAGE_ACCEPTED;
    },

    parse: function (data) {
      let res = {
        width: getI32LE(data, 18),
        height: getI32LE(data, 22),
      };
      if (res.width < 0) res.width = -res.width;
      if (res.height < 0) res.height = -res.height;
      return res;
    },
  },
  // webp (dunno why)
  {
    name: "WEBP",
    minDetectBytes: 16,
    minHeaderBytes: 30,
    maxHeaderBytes: 30,

    detect: function (data) {
      if (data[0] !== 'R' || data[1] !== 'I' || data[2] !== 'F' || data[3] !== 'F') return IMAGE_REJECTED;
      if (data[8] !== 'W' || data[9] !== 'E' || data[10] !== 'B' || data[11] !== 'P') return IMAGE_REJECTED;
      if (data[12] !== 'V' || data[13] !== 'P' || data[14] !== '8') return IMAGE_REJECTED;
      if (data[15] !== ' ' && data[15] !== 'L' && data[15] !== 'X') return IMAGE_INVALID;
      return IMAGE_ACCEPTED;
    },

    parse: function (data) {
      switch (data[15]) {
        case 'X': // extended
          return {
            width: getU24LE(data, 24)+1,
            height: getU24LE(data, 27)+1,
          };
        case 'L': // lossless
          if (data[20] !== '\x2f') return IMAGE_INVALID; // signature
          {
            let b1 = getU8(data, 21);
            let b2 = getU8(data, 22);
            let b3 = getU8(data, 23);
            let b4 = getU8(data, 24);
            return {
              width: (((b2&0x3f)<<8)|b1)+1,
              height: (((b4&0x0f)<<10)|(b3<<2)|((b2&0xc0)>>6))+1,
            };
          }
        case ' ': // lossy
          // 3 bytes: frame tag
          // 0x9d, 0x01, 0x2a
          // u16 w&3ffff, u16h&3ffff
          if (data[23] !== '\x9d' || data[24] !== '\x01' || data[25] !== '\x2a') return IMAGE_INVALID;
          return {
            width: getU16LE(data, 26)&0x3fff,
            height: getU16LE(data, 28)&0x3fff,
          };
      }
      return IMAGE_INVALID; // just in case
    },
  },
];


////////////////////////////////////////////////////////////////////////////////
function FormatChecker () {
  this.formats = ImageFormats.slice();
  // prepare some internal data
  let maxHeaderBytes = 0;
  for (let fmt of this.formats) {
    if (!fmt) continue;
    if (typeof(fmt) !== "object") continue;
    fmt.accepted = false; // `true` means that format is accepted, but size is not determined yet
    if (maxHeaderBytes < fmt.maxHeaderBytes) maxHeaderBytes = fmt.maxHeaderBytes;
  }
  if (maxHeaderBytes < 1) throw new Error("WTF?!");
  this.maxHeaderBytes = maxHeaderBytes;
}


FormatChecker.prototype = {
  // `true`: no more checkers, unknown format
  get done () (this.formats.length === 0),

  // check buffer
  // returns:
  //  `null` -- no more possible formats
  //  `true` -- need more data
  //  {string name, int width, int height, valid:true} -- format detected
  //  {string name, valid:false} -- format detected as invalid
  process: function (data) {
    if (this.formats.length < 1) return null; // no more formats
    // check formats
    for (let idx = 0; idx < this.formats.length; ++idx) {
      let fmt = this.formats[idx];
      // need to do detection?
      if (!fmt.accepted) {
        if (data.length < fmt.minDetectBytes) continue;
        let res = fmt.detect(data);
        if (res === IMAGE_NEED_MORE_DATA) continue;
        if (res === IMAGE_INVALID) {
          this.formats = [];
          return {name:fmt.name, valid:false};
        }
        if (res === IMAGE_REJECTED) {
          //conlog("'", fmt.name, "' rejected");
          // remove it
          this.formats.splice(idx, 1);
          --idx;
          continue;
        }
        if (res !== IMAGE_ACCEPTED) throw new Error("'"+fmt.name+"' detector returns invalid result");
        fmt.accepted = true;
      }
      // try to parse format
      if (data.length >= fmt.minHeaderBytes) {
        let res = fmt.parse(data);
        if (res === IMAGE_INVALID) {
          this.formats = [];
          return {name:fmt.name, valid:false};
        }
        if (res === IMAGE_REJECTED) {
          //conlog("'", fmt.name, "' rejected");
          // remove it
          this.formats.splice(idx, 1);
          --idx;
          continue;
        } else if (typeof(res) === "object") {
          if (!isNaN(res.width) && !isNaN(res.height)) {
            // format detected!
            this.formats = [];
            res.name = fmt.name;
            res.valid = true;
            return res;
          }
        } else if (res !== IMAGE_ACCEPTED && res !== IMAGE_NEED_MORE_DATA) {
          throw new Error("'"+fmt.name+"' parser returns invalid result");
        }
      }
      // reject this format if header is too big, and can't parse yet
      if (data.length > fmt.maxHeaderBytes) {
        this.formats.splice(idx, 1);
        --idx;
      }
    }
    // no suitable format found
    if (data.length > this.maxHeaderBytes) this.formats = []; // alas
    return (this.formats.length < 1 ? null : true);
  },
};
