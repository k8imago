/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
var EXPORTED_SYMBOLS = [];

let {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;

//////////////////////////////////////////////////////////////////////////////
Cu.import("chrome://k8-imago-code/content/modules/utils.js");

//Cu.import(MODULE_PATH+"imago-policy.js");

Cu.import(MODULE_PATH+"debuglog.js");

// autoinit
Cu.import(MODULE_PATH+"prefs.js");
Cu.import(MODULE_PATH+"httpobserver.js");
Cu.import(MODULE_PATH+"cmenu.js");

// other (in case they want inits too)
Cu.import(MODULE_PATH+"rule-engine.js");
Cu.import(MODULE_PATH+"detector.js");
Cu.import(MODULE_PATH+"hazard.js");
Cu.import(MODULE_PATH+"imgreload.js");
Cu.import(MODULE_PATH+"stoplist.js");
Cu.import(MODULE_PATH+"tamper.js");
