/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
// traffic tamper
var EXPORTED_SYMBOLS = [
  "ImgDetectListener"
];

let {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;


//////////////////////////////////////////////////////////////////////////////
Cu.import("resource://gre/modules/Services.jsm");

Cu.import("chrome://k8-imago-code/content/modules/utils.js");
Cu.import(MODULE_PATH+"debuglog.js");
Cu.import(MODULE_PATH+"prefs.js");
Cu.import(MODULE_PATH+"detector.js");
Cu.import(MODULE_PATH+"hazard.js");
Cu.import(MODULE_PATH+"stoplist.js");


//////////////////////////////////////////////////////////////////////////////
// this listener will collect first several kb of image, detect it's type and
// size, and then will decide what to do with it (image, not size)
let tmpid = 0;


function ImgDetectListener (isoctet, blacklisted, doSizeBlock) {
  this.olst = null;
  this.receivedData = ""; // string, incoming data; will become 0
  this.cancelled = false;
  this.piping = false;
  this.stopSent = false;
  this.octets = !!isoctet;
  this.blacklisted = !!blacklisted;
  this.maxLength = PREFS.maxLength;
  this.checker = new FormatChecker();
  this.id = tmpid++;
  this.imageInfo = null;
  this.doSizeBlock = !!doSizeBlock;
  this.rule = null;
  this.mainURI = null; // URI of the main window
}


ImgDetectListener.prototype = {
  get debugLogOpt () (PREFS.debugLog),

  get minWidthOpt () (this.rule ? this.rule.minWidth : PREFS.minWidth),
  get minHeightOpt () (this.rule ? this.rule.minHeight : PREFS.minHeight),
  get maxWidthOpt () (this.rule ? this.rule.maxWidth : PREFS.maxWidth),
  get maxHeightOpt () (this.rule ? this.rule.maxHeight : PREFS.maxHeight),
  get showPlaceholderOpt () (this.rule ? this.rule.showPlaceholder : PREFS.showPlaceholder),
  get allowUnknownFormatsOpt () (this.rule ? this.rule.allowUnknownFormats : PREFS.allowUnknownFormats),
  get maxLengthOpt () (this.rule ? this.rule.maxLength : PREFS.maxLength),
  get allowFirstPartyImagesOpt () (this.rule ? this.rule.allowFirstPartyImages : PREFS.allowFirstPartyImages),


  log: function () {
    if (!this.debugLogOpt) return;
    if (arguments.length) {
      let s = ""+this.id+":: ";
      for (let idx = 0; idx < arguments.length; ++idx) s += arguments[idx];
      Services.console.logStringMessage(s);
    }
  },

  logError: function () {
    if (!this.debugLogOpt) return;
    if (arguments.length) {
      let s = ""+this.id+":: ";
      for (let idx = 0; idx < arguments.length; ++idx) s += arguments[idx];
      Cu.reportError(s);
    }
  },

  _markImage: function (request) {
    let srcchan = request.QueryInterface(Ci.nsIChannel);
    let url = srcchan.URI.spec;
    let dw = getDomWindowForChannel(srcchan);
    if (dw) {
      //Services.console.logStringMessage("==================== <"+url+">");
      let title;
      if (this.imageInfo) {
        title = this.imageInfo.name+" "+(this.imageInfo.valid ? ""+this.imageInfo.width+"x"+this.imageInfo.height : "<INVALID>");
      } else {
        title = (this.blacklisted ? "<BLACKLISTED>" : "<INVALID>");
      }
      //for (let img of dw.document.querySelectorAll("img:not([k8imago-mark])[src=\""+cssEscape(url)+"\"]")) {
      for (let img of dw.document.querySelectorAll("img")) {
        //for (let iidx = 0; iidx < dw.document.images.length; ++iidx) {
          //let img = dw.document.images[iidx];
        if (img.src == url) {
          img.setAttribute("k8imago-saved-title", (img.getAttribute("title")||""));
          img.setAttribute("k8imago-saved-alt", (img.getAttribute("alt")||""));
          img.setAttribute("title", title);
          img.setAttribute("alt", title);
          img.setAttribute("k8imago-mark", "tan");
          //if (PREFS.debugLog) conlog("!!! setting title for <"+url+">");
          //Services.console.logStringMessage("!!! setting title for <"+url+">");
        }
        /*
        else {
          Services.console.logStringMessage("!!! SKIPPING title for <"+img.src+">");
        }
        */
      }
      //Services.console.logStringMessage("=======================================================");
    }
    /*
    else {
      Services.console.logStringMessage("+++***+++ NO DOM WINDOW FOR <"+url+">");
    }
    */
  },

  // cancel request
  _cancel: function (request, context, total, data) {
    if (typeof(total) !== "number") total = "???";
    // abort this request
    this.cancelled = true;
    this.receivedData = null; // just in case
    this.checker = null;
    this._markImage(request);
    if (this.debugLogOpt) {
      let srcchan = request.QueryInterface(Ci.nsIChannel);
      let url = srcchan.URI.spec;
      this.log("[", url, "] total: ", total, ": CANCELLING!");
    }
    if (typeof(data) === "string") {
      if (this.showPlaceholderOpt) {
        this.log(" sending HAZARD");
        this.olst.onDataAvailable(request, context, createInputStreamFromString(data), 0, data.length);
      } else {
        this.log(" sending HAZARD is blocked");
      }
      this.stopSent = true;
      this.olst.onStopRequest(request, context, 0);
    }
    // just in case that double cancel will throw
    try {
      request.cancel(Cr.NS_BINDING_ABORTED);
    } catch (e) {}
  },

  _pipe: function (request, context) {
    this.piping = true;
    this.receivedData = null;
    this.checker = null;
  },

  url: function (request) {
    if (!request) return "<no-url>";
    let srcchan = request.QueryInterface(Ci.nsIChannel);
    if (!srcchan) return "<no-url>";
    return srcchan.URI.spec;
  },

  // object initialization finished
  inited: function () {
  },

  QueryInterface: function (aIID) {
    if (aIID.equals(Ci.nsIStreamListener) || aIID.equals(Ci.nsISupports)) return this;
    throw Components.results.NS_NOINTERFACE;
  },

  onDataAvailable: function (request, context, inputStream, offset, count) {
    if (this.cancelled) return;

    if (!this.piping && this.debugLogOpt) this.log("[", this.url(request), "] offset=", offset, "; count=", count);

    // if we aren't piping...
    if (!this.piping) {
      // ...and there's no more formats...
      if (this.checker.done) {
        // abort or pipe
        if (!this.allowUnknownFormatsOpt) {
          this._cancel(request, context, offset+count, hazardPNG);
          return;
        }
        // send collected data
        if (this.receivedData) {
          if (this.receivedData.length) {
            if (offset == 0) this.logError("WTF000?! offset is zero, but we have receivedData!");
            this.olst.onDataAvailable(request, context, createInputStreamFromString(this.receivedData), 0, this.receivedData.length);
            if (offset != this.receivedData.length) this.logError("WTF001?! offset is not equal to receivedData length! offset=", offset, "; length=", this.receivedData.length);
          }
        } else if (offset > 0) {
          this.logError("WTF002?! offset is not zero, but we have no receivedData!");
        }
        this._pipe(request, context);
        // go on, do piping
      }
    }

    // pipe data if we are piping
    if (this.piping) {
      // check if we hit the limit
      if (this.doSizeBlock) {
        let srcchan = request.QueryInterface(Ci.nsIChannel);
        if (this.maxTotalBytesForDocOpt > 0 && !this.octets) {
          let dw = getDomWindowForChannel(srcchan);
          let isize = getByteCounterForDomWindow(dw)+count;
          if (isize >= this.maxTotalBytesForDocOpt) {
            if (this.debugLogOpt) conlog("*** [", httpChannel.URI.spec, "]: blocked due to bytes limit: isize="+isize+"; max="+this.maxTotalBytesForDocOpt+"; count="+count);
            this._cancel(request, context, offset+count);
            return;
          }
          setByteCounterForDomWindow(dw, isize);
        }
      }
      // piping data
      if (this.maxLength > 0 && !this.octets) {
        if (offset+count > this.maxLength) {
          this._cancel(request, context, offset+count);
          return;
        }
      }
      this.olst.onDataAvailable(request, context, inputStream, offset, count);
      return;
    }

    // collecting header data and analyzing image formats
    let ist = Cc["@mozilla.org/binaryinputstream;1"].createInstance(Ci.nsIBinaryInputStream);
    ist.setInputStream(inputStream);

    // read up to `this.checker.maxHeaderBytes` kb
    let maxb = this.checker.maxHeaderBytes;
    if (this.receivedData.length+count < maxb) {
      this.log(" reading ", count, " bytes (maxb=", maxb, ")");
      this.receivedData += ist.readBytes(count);
      count = 0;
    } else {
      let left = maxb-this.receivedData.length;
      if (left > count) {
        Cu.reportError("internal error (something is heavily fucked, #000)");
        throw new Error("internal error (something is heavily fucked, #000)");
      }
      if (left > 0) {
        this.receivedData += ist.readBytes(left);
        count -= left;
      }
      this.log(" read ", left, " bytes, ", count, " bytes left");
    }

    let res = this.checker.process(this.receivedData);
    this.log(" checker returned `", res, "`, done is: ", this.checker.done);
    if (res === true) {
      // no successfull detection yet, need more spice
      if (this.receivedData.length >= maxb) this._cancel(request, context, offset+count, hazardPNG);
      return;
    }
    if (res !== null) {
      // detected something
      this.imageInfo = res;
      if (!res.valid) {
        this._cancel(request, context, offset+count, hazardPNG);
        return;
      }
      this.log(" image: ", res.name, ": ", res.width, "x", res.height);
      // valid image, check dimensions
      if (res.width < this.minWidthOpt || res.height < this.minHeightOpt ||
          res.width > this.maxWidthOpt || res.height > this.maxHeightOpt)
      {
        this.log(" allowed size: min=", this.minWidthOpt, "x", this.minHeightOpt, "; max=", this.maxWidthOpt, "x", this.maxHeightOpt);
        this._cancel(request, context, offset+count, hazardPNG);
        return;
      }
    } else {
      // no more formats
      if (!this.allowUnknownFormatsOpt) {
        this.log(" unknown format");
        this._cancel(request, context, offset+count, hazardPNG);
        return;
      }
    }

    // here we got something good, transfer collected header and start piping
    this.olst.onDataAvailable(request, context, createInputStreamFromString(this.receivedData), 0, this.receivedData.length);
    // pass what is left
    if (count > 0) {
      offset += this.receivedData.length;
      this.olst.onDataAvailable(request, context, inputStream, offset, count);
    }
    this._pipe(request, context);
  },

  onStartRequest: function (request, context) {
    if (this.debugLogOpt) this.log("onStartRequest() [", this.url(request), "]");
    this.olst.onStartRequest(request, context);
    if (this.blacklisted) {
      this.log("image is blacklisted");
      this._cancel(request, context, 0, hazardPNG);
      return;
    }
  },

  onStopRequest: function (request, context, statusCode) {
    if (this.debugLogOpt) this.log("onStopRequest() [", this.url(request), "]: status code=", statusCode);
    //this.log("[", this.url(request), "]: status code=", statusCode);
    if (!this.stopSent && this.receivedData !== null) {
      // wow, we have some buffered data, check and send it!
      let res = this.checker.process(this.receivedData);
      if (typeof(res) === "object") {
        if (!res.valid) {
          this._cancel(request, context, 0, hazardPNG);
          return;
        }
        if (res.width < this.minWidthOpt || res.height < this.minHeightOpt ||
            res.width > this.maxWidthOpt || res.height > this.maxHeightOpt) {
          this._cancel(request, context, 0, hazardPNG);
          return;
        }
        // ok
      } else {
        if (!this.allowUnknownFormatsOpt) {
          this._cancel(request, context, 0, hazardPNG);
          return;
        }
      }
      this.olst.onDataAvailable(request, context, createInputStreamFromString(this.receivedData), 0, this.receivedData.length);
    }
    this.receivedData = null; // just in case
    this.checker = null;
    if (!this.stopSent) {
      this.olst.onStopRequest(request, context, statusCode);
    }
  },
};
