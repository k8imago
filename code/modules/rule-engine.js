/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
// reload image
var EXPORTED_SYMBOLS = [
  "RuleUrl",
  "Rule",
  "RuleSet",
  "getRuleForURI"
];

let {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;


//////////////////////////////////////////////////////////////////////////////
Cu.import("chrome://k8-imago-code/content/modules/utils.js");
Cu.import(MODULE_PATH+"debuglog.js");
Cu.import(MODULE_PATH+"prefs.js");
let debugRules = false;


//////////////////////////////////////////////////////////////////////////////
/*
rule engine
===========

main:urx
  the following rules will be applied only if "main" URI matches

action:[!]host
  NO WILDCARDS!
  if "host" starts with ".": all subdomains

action:[!]/regexp/[flags]
  check whole URL
  flags:
    s -- include scheme
    a -- include hash
    h -- include host without port
    P -- exclude path
    : -- include host with port
    i -- case-insensitive

action:=url
  compare full url, literally

action:default

actions:
  allow
  deny
 (will not stop processing)
  allow-1stparty
  deny-1stparty
  allow-unknown
  deny-unknown
  minsize[<wdt>x<hgt>]
  maxsize[<wdt>x<hgt>]

default action: use global flags (allow 1st-party, etc)
*/

////////////////////////////////////////////////////////////////////////////////
// rule url
//  string type ("regexp", "host", "host-tail", "literal", "default")
//  bool negative
//  bool function match (uri) -- must be nsIURI, returns true/false
function RuleUrl (pat) {
  if (typeof(pat) !== "string") throw new Error("invalid pattern type");
  this.parse(pat);
}

RuleUrl.prototype = {
  parse: function (pat) {
    if (pat.length === 0) throw new Error("invalid pattern");
    this.negative = (pat[0] == "!");
    if (this.negative) pat = pat.substr(1);

    if (pat.length === 0) throw new Error("invalid pattern");
    if (pat === "default") {
      this.type = "default";
      return;
    }
    switch (pat[0]) {
      case "/": this.type = "regexp"; this.parseRegexp(pat.substr(1)); break;
      case "=": this.type = "literal"; this.parseLiteral(pat.substr(1)); break;
      case ".": this.type = "host-tail"; this.parseHostTail(pat.substr(1)); break;
      default: this.type = "host"; this.parseHost(pat); break;
    }
    //conlog("type: ", this.type);
  },

  parseLiteral: function (pat) {
    this.literal = pat;
  },

  parseHost: function (pat) {
    this.host = pat.toLowerCase();
  },

  parseHostTail: function (pat) {
    this.host = pat.toLowerCase();
  },

  parseRegexp: function (pat) {
    let mt = pat.match(/^(.+)\/([sahP:i]*)$/);
    if (!mt) throw new Error("invalid regexp pattern");
    let opts = mt[2];
    //conlog("opts: [", opts, "]; re=", mt[1]);
    this.useScheme = (opts.indexOf("s") >= 0);
    this.useHash = (opts.indexOf("a") >= 0);
    this.useHost = (opts.indexOf("h") >= 0);
    this.usePath = (opts.indexOf("P") < 0);
    this.usePort = (opts.indexOf(":") >= 0);
    let reopt = (opts.indexOf("i") >= 0 ? "i" : "");
    this.regexp = new RegExp(mt[1], reopt);
  },

  match: function (uri) {
    try {
      let res = false;
      switch (this.type) {
        case "literal": res = (uri.spec == this.literal); break;
        case "host":
          res = (uri.host == this.host);
          //conlog(" HOST: [", uri.host, " : ", this.host, "] res=", res);
          break;
        case "host-tail": {
          let h = uri.host;
          let ph = this.host;
          let idx = h.lastIndexOf(ph);
          res = (idx === 0 || (idx > 0 && h[idx-1] === "."));
          } break;
        case "regexp": {
            let str = "";
            // whole?
            if (this.useScheme && this.usePort && this.usePath) {
              str = uri.spec;
            } else {
              if (this.useScheme) str += uri.scheme+"://";
              // "nohost" can pappen for "chrome:", for exampe
              if (this.usePort) str += uri.hostPort; else if (this.useHost) str += (uri.host ? uri.host : "nohost");
              if (this.usePath) str += uri.path;
            }
            if (!this.useHash) {
              let idx = str.indexOf("#");
              if (idx >= 0) str = str.substr(0, idx);
            }
            //conlog("str=[", str, "]");
            res = this.regexp.test(str);
          } break;
        case "default": res = true; break;
        default: return false;
      }
      return (this.negative ? !res : res);
    } catch (e) {
      logException("FUCK ("+this.type+" "+uri.spec+")", e);
    }
    return false;
  },

  equal: function (obj) {
    if (typeof(obj) !== "object") return false;
    if (obj.type !== this.type) return false;
    if (obj.negative !== this.negative) return false;
    try {
      switch (this.type) {
        case "default": return true;
        case "literal": return (obj.literal === this.literal);
        case "host": case "host-tail": return (obj.host === this.host);
        case "regexp": return (obj.regexp.flags === this.regexp.flags && obj.regexp.source === this.regexp.source);
      }
    } catch (e) {}
    return false;
  },

  toString: function () {
    let res = "RuleUrl<"+(this.negative ? "not:" : "")+"type:"+this.type+":";
    switch (this.type) {
      case "default": res += "<default>"; break;
      case "literal": res += this.literal; break;
      case "host": case "host-tail": res += this.host; break;
      case "regexp": res += this.regexp.flags+":"+this.regexp.source; break;
    }
    return res+">";
  },
};


////////////////////////////////////////////////////////////////////////////////
// rule object
function Rule (text) {
  this.options = {
    action: "allow",
    stopAction: true,
  };
  this.isDefault = false;
  this.isMain = false;
  if (typeof(text) === "string") this.parse(text);
}

Rule.prototype = {
  get minWidth () ("minWidth" in this.options ? this.options.minWidth : PREFS.minWidth),
  get minHeight () ("minHeight" in this.options ? this.options.minHeight : PREFS.minHeight),
  get maxWidth () ("maxWidth" in this.options ? this.options.maxWidth : PREFS.maxWidth),
  get maxHeight () ("maxHeight" in this.options ? this.options.maxHeight : PREFS.maxHeight),
  get showPlaceholder () ("showPlaceholder" in this.options ? this.options.showPlaceholder : PREFS.showPlaceholder),
  get allowUnknownFormats () ("allowUnknownFormats" in this.options ? this.options.allowUnknownFormats : PREFS.allowUnknownFormats),
  get maxLength () ("maxLength" in this.options ? this.options.maxLength : PREFS.maxLength),
  get allowFirstPartyImages () ("allowFirstPartyImages" in this.options ? this.options.allowFirstPartyImages : PREFS.allowFirstPartyImages),
  get action () (this.options.stopAction ? this.options.action : "allow"),
  get stopAction () this.options.stopAction,

  parse: function (text) {
    //conlog("text: ", text);
    let mt = text.match(/^\s*([^\s:]+)\s*:\s*(.+?)\s*$/);
    if (!mt) throw new Error("invalid rule");
    let action = mt[1];
    let url = mt[2];
    if (url === "default") this.isDefault = true;
    this.options.stopAction = false;
    // "main:" rule?
    if (action === "main") {
      this.urlRule = new RuleUrl(url);
      this.isMain = true;
      return;
    }
    // other rules
    //conlog("action=<", action, ">; url=<", url, ">");
    switch (action) {
      case "allow": this.options.action = "allow"; this.options.stopAction = true; break;
      case "deny": this.options.action = "deny"; this.options.stopAction = true; break;
      //case "default": this.isDefault = true; this.options.action = (url === "allow" ? "allow" : "deny"); this.options.stopAction = true; break;
      case "allow-1stparty": this.options.allowFirstPartyImages = true; break;
      case "deny-1stparty": this.options.allowFirstPartyImages = false; break;
      case "allow-unknown": this.options.allowUnknownFormats = true; break;
      case "deny-unknown": this.options.allowUnknownFormats = false; break;
        break;
      default: {
          let mt = action.match(/^(m(?:in|ax))\-?size\s*\[\s*(\d+)\s*[Xx]\s*(\d+)\s*\]$/);
          if (!mt) throw new Error("invalid rule");
          //conlog(mt[1], ": ", mt[2], "x", mt[3]);
          if (mt[1] === "min") {
            this.options.minWidth = parseInt(mt[2], 10);
            this.options.minHeight = parseInt(mt[3], 10);
          } else {
            this.options.maxWidth = parseInt(mt[2], 10);
            this.options.maxHeight = parseInt(mt[3], 10);
          }
          //this.options.action = mt[1]+"-size";
        } break;
    }
    if (url === "default") {
      this.isDefault = true;
      this.urlRule = new RuleUrl("default");
    } else {
      this.urlRule = new RuleUrl(url);
    }
  },

  copyOptionsFrom: function (rule, replaceExisting) {
    if (!(rule instanceof Rule)) throw new Error("Rule object expected");
    replaceExisting = !!replaceExisting;
    for (let [n, v] of Iterator(rule.options)) {
      if (n === "action" || n === "stopAction") continue;
      if (replaceExisting || !(n in this.options)) {
        this.options[n] = v;
      }
    }
    if (rule.isDefault) this.isDefault = true;
  },

  toString: function () {
    let res = "Rule<"+(this.isDefault ? "*" : "")+(this.urlRule ? this.urlRule.toString()+"|" : "")+(JSON.stringify(this.options)+">");
    return res;
  },
};


////////////////////////////////////////////////////////////////////////////////
// ruleset object
function RuleSet () {
  this.rules = [];
  this.defaultRule = null;
}

RuleSet.prototype = {
  addRule: function (rule) {
    let rs = rule;
    if (typeof(rule) === "string") rule = new Rule(rule);
    if (!(rule instanceof Rule)) throw new Error("Rule object expected");
    if (debugRules) conlog("* got rule: ", rule, " from: ", rs);
    // find rule with same url and combine
    for (let [idx, r] of Iterator(this.rules)) {
      if (r.urlRule.equal(rule.urlRule)) {
        if (r.stopAction) {
          // this rule is no-op
          if (debugRules) conlog("*  ignored rule due to: ", r);
          return;
        }
        if (debugRules) conlog("*  squashing rules: ", r);
        // if we get stopAction rule, copy options, remove found rule, and push new one
        if (rule.stopAction) {
          rule.copyOptionsFrom(r, false); // don't overwrite
          this.rules.splice(idx, 1);
          this.rules.push(rule);
          r = rule;
        } else {
          // combine rules
          r.copyOptionsFrom(rule, true); // overwrite
        }
        if (r.isDefault) this.defaultRule = r;
        if (debugRules) conlog("*  resulting rule: ", r);
        return;
      }
    }
    // not found: simply push
    this.rules.push(rule);
    if (rule.isDefault) this.defaultRule = rule;
  },

  // nsIURI
  matchURI: function (uri, mainUri) {
    let skipMain = false;
    for (let r of this.rules) {
      if (r.isMain) {
        let oldSkip = skipMain;
             if (r.isDefault) skipMain = false;
        else if (mainUri) skipMain = !r.urlRule.match(mainUri);
        else skipMain = true;
        continue;
      }
      if (!skipMain) {
        if (r.urlRule.match(uri)) return r;
      }
    }
    if (!this.defaultRule) this.defaultRule = new Rule("allow:default");
    return this.defaultRule;
  },
};


//////////////////////////////////////////////////////////////////////////////
let currentRules = null;
let ruleIFile = getRulesIFile();
let lastTime = -1;


function updateRulesFile () {
  //conlog(":", ruleIFile.path);
  if (!ruleIFile.exists() || !ruleIFile.isReadable()) {
    currentRules = null;
    lastTime = -1;
    return;
  }
  let mt = ruleIFile.lastModifiedTime;
  if (mt === lastTime) return;
  conlog("IMAGO: reloading rules!");
  lastTime = mt;
  // load rules
  try {
    let rl = new RuleSet();
    let src = getFileTextContents(ruleIFile);
    for (let line of src.split("\n")) {
      line = line.replace(/^\s+/, "").replace(/\s+$/, "");
      if (!line || line[0] === "#") continue;
      rl.addRule(line);
    }
    currentRules = rl;
  } catch (e) {
    conlog("IMAGO RULES ERROR! ", e.message);
    currentRules = null;
  }
}


function getRuleForURI (uri, mainUri) {
  updateRulesFile();
  return (currentRules ? currentRules.matchURI(uri, mainUri) : null);
}
