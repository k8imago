/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
// loading policy
let {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;

//////////////////////////////////////////////////////////////////////////////
Cu.import("resource://gre/modules/XPCOMUtils.jsm");
Cu.import("resource://gre/modules/Services.jsm");

//////////////////////////////////////////////////////////////////////////////
Cu.import("chrome://k8-imago-code/content/modules/utils.js");
Cu.import(MODULE_PATH+"debuglog.js");
Cu.import(MODULE_PATH+"prefs.js");


//////////////////////////////////////////////////////////////////////////////
function Policy () {
  // will be called on creating new component
  //conlog("IMAGO POLICY ctor!");
}

Policy.prototype = {
  classDescription: "Imago Policy",
  classID: Components.ID("{085ef727-7222-4ad7-8fe9-653f73d6ee2f}"),
  contractID: "@ketmar.no-ip.org/k8imago-policy;1",
  _xpcom_categories: [{
    category: "content-policy",
    entry: "imago-policy",
  }],

  QueryInterface: XPCOMUtils.generateQI([Ci.nsIContentPolicy, Ci.nsIFactory]),

  createInstance: function (outer, iid) {
    conlog("Policy: createInstance");
    if (outer) throw Cr.NS_ERROR_NO_AGGREGATION;
    return this.QueryInterface(iid);
  },

  // nsIContentPolicy interface implementation
  shouldLoad: function (contentType, contentLocation, requestOrigin, node, mimeTypeGuess, extra) {
    if (contentType !== Ci.nsIContentPolicy.TYPE_IMAGE) return Ci.nsIContentPolicy.ACCEPT;
    /*
    conlog("shouldLoad: ", contentType, " ",
           (contentLocation ? contentLocation.spec : "null"), " ",
           (requestOrigin ? requestOrigin.spec : "null"), " ",
           node, " ",
           mimeTypeGuess, " <", contentType !== Ci.nsIContentPolicy.TYPE_IMAGE, ">");
    */
    if (!contentLocation) return Ci.nsIContentPolicy.ACCEPT; // the thing that should not be
    try {
      //conlog("shouldLoad000: ", contentLocation.spec, " ", contentLocation);
      //if (typeof(contentLocation.host) !== "string") return Ci.nsIContentPolicy.ACCEPT; // some strange things
      //conlog("shouldLoad001: ", contentLocation.host);
      if (BLACK_HOSTS[contentLocation.host]) {
        //TODO: list priorities
        if (WHITE_HOSTS[contentLocation.host]) return Ci.nsIContentPolicy.ACCEPT;
        conlog("REJECTED BLACK: ", contentLocation.host);
        return Ci.nsIContentPolicy.REJECT;
      }
    } catch (e) {
      //conlog("************");
      //Cu.reportError(e);
    }
    //conlog("++++++++++++");
    return Ci.nsIContentPolicy.ACCEPT;
  },

  shouldProcess: function (contentType, contentLocation, requestOrigin, node, mimeTypeGuess, extra) {
    /*
    conlog("shouldProcess: ", contentType, " ",
                            (contentLocation ? contentLocation.spec : "null"), " ",
                            (requestOrigin ? requestOrigin.spec : "null"), " ",
                            node, " ",
                            mimeTypeGuess);
    */
    return Ci.nsIContentPolicy.ACCEPT;
  },

/*
  _initialized: false,

  dynreg: function () {
    if (this._initialized) return;
    this._initialized = true;

    conlog("initializing Imago component...");
    let registrar = Components.manager.QueryInterface(Ci.nsIComponentRegistrar);
    registrar.registerFactory(this.classID, this.classDescription, this.contractID, this);

    let catMan = Cc["@mozilla.org/categorymanager;1"].getService(Ci.nsICategoryManager);
    for each (let xpc in this._xpcom_categories) {
      //catMan.addCategoryEntry(xpc.category, this.contractID, this.contractID, false, true);
      catMan.addCategoryEntry(xpc.category, xpc.entry, this.contractID, false, true);
    }
  },
*/
};


//////////////////////////////////////////////////////////////////////////////
// dynamic registration: change above to `let Policy = {}`
//Policy.dynreg();
// manifest registration
let NSGetFactory = XPCOMUtils.generateNSGetFactory([Policy]);
//conlog("IMAGO POLICY1");
