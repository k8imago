#!/bin/sh

odir=`pwd`
mdir=`dirname "$0"`
cd "$mdir"

rm -rf _build 2>/dev/null
mkdir _build

zip -9r _build/imago@ketmar.no-ip.org.xpi \
  chrome.manifest \
  install.rdf \
  chrome/* \
  code/* \
  defaults/* \


advzip -z4 _build/imago@ketmar.no-ip.org.xpi
